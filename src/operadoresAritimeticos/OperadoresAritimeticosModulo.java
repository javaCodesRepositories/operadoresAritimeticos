package operadoresAritimeticos;

public class OperadoresAritimeticosModulo {

	public static void main(String[] args) {

		/* Modulo eh o Resto da divisao. (Operador % ou MOD) */
		double carros = 9;
		double pessoas = 2;

		double resultado = carros % pessoas;
		System.out.println("Sobraram exatamente: " + resultado + " carros.");

	}
}
