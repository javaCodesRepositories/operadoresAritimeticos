package operadoresAritimeticos;

public class OperadoresAritimeticos {

	public static void main(String[] args) {

		double n1 = 50;
		double n2 = 78;

		System.out.println("Adicao = " + (n1 + n2));
		System.out.println("Subtracao = " + (n1 - n2));
		System.out.println("Subtracao = " + (n2 - n1));
		System.out.println("Multiplicacao = " + (n1 * n2));
		System.out.println("Divisao = " + (n1 / n2));

	}
}
